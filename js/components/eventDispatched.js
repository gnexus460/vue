window.Event = new Vue();

Vue.component('coupon', {
    template: `<input type="text" placeholder="Enter your coupon code" @blur="applyCoupon">`,
    methods: {
        applyCoupon() {
            this.$emit('applied')
        }
    }
});

new Vue({
    el: '#root',
    data: {
        couponApplied: false
    },
    created() {
        Event.$on('applied', () => {
            alert('Handle')
        })
    },
    // methods: {
    //     onCouponApplied() {
    //         this.couponApplied = true;
    //
    //         this.$on('applied', function () {
    //
    //         });
    //     }
    // }
});
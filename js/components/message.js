Vue.component('message', {
    props: ['title', 'body'],
    data() {
        return {
            isVisible: true
        }
    },
    template: `
        <div>
            <transition name="fade">
                <article class="message" v-show="isVisible">
                    <div class="message-header">
                        <p>{{title}}</p>
                        <button @click="hideModal" class="delete" aria-label="delete"></button>
                    </div>
                    <div class="message-body">
                    {{body}}
                    </div>
                </article>
            </transition>
            <button @click="hideModal">hide</button>
        </div>
    `,
    methods: {
        hideModal() {
            this.isVisible = !this.isVisible;
        }
    }
});

new Vue({
    el: '#root'
})
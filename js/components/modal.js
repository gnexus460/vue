Vue.component('modal', {
    template: `
    <div>
        <div class="modal is-active">
          <div class="modal-background" @click="$emit('close')"></div>
          <div class="modal-content">
            <div class="box">
                <slot></slot>
            </div>
          </div>
          <button class="modal-close is-large" @click="$emit('close')" aria-label="close"></button>
        </div>
    </div>
    `,

});

new Vue({
    el: '#root',
    data() {
        return {
            isActive: false,
        }
    }
});
Vue.component('task-list', {
    template: '<div><task v-for="task in tasks" :key="task.description">{{task.description}}</task></div>',
    data() {
        return {
            tasks: [
                {
                    description: 'Go to the store',
                    completed: false
                },
                {
                    description: 'Go to the store2',
                    completed: true
                },
                {
                    description: 'Go to the store3',
                    completed: true
                }
            ]
        }
    }
});

Vue.component('task', {
    template: '<li><slot></slot></li>'
});

new Vue({
    el: '#root'
});
<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
    <title>Document</title>
</head>
<style>
    .completed {
        text-decoration: line-through;
    }
    .fade-enter-active, .fade-leave-active {
        transition: opacity .3s;
    }
    .fade-enter, .fade-leave-to /* .fade-leave-active до версии 2.1.8 */ {
        opacity: 0;
    }
</style>
<body>

<div id="root" class="container">
    <?php
    $items = [0,2,3,4,5,6,7];
    ?>
    <progress-view v-bind:array="<?php echo json_encode($items); ?>" inline-template>
        <div>

            <?php foreach ($items as $item):?>
                <div>
                    <h1>Your progress Through This Course Is {{ completionRate }}</h1>
                </div>
            <?php endforeach;?>
        </div>
    </progress-view>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="js/components/inlineTemplates.js"></script>
</body>
</html>